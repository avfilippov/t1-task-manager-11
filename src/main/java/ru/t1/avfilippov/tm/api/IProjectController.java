package ru.t1.avfilippov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void showProjectById();

    void showProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
